//
//  InputNameBuilder.swift
//   2020
//

import RIBs
import UserInterface

public protocol InputNameDependency: AuthenticationCoreDependency { }

final class InputNameComponent: Component<InputNameDependency> { }

// MARK: - Builder

protocol InputNameBuildable: Buildable {
    func build(withListener listener: FormListener?, mode: EditingMode) -> InputNameRouting
}

final class InputNameBuilder: Builder<InputNameDependency>, InputNameBuildable {

    func build(withListener listener: FormListener?, mode: EditingMode = .update) -> InputNameRouting {
        let viewController = InputNameViewController(themeStream: dependency.themeStream, keyboardObserver: dependency.keyboardObserver, mode: mode)
        let interactor = InputNameInteractor(presenter: viewController, authenticationService: dependency.authenticationService, permissionsManager: dependency.permissionsManager, userService: dependency.userService)
        interactor.listener = listener
        return InputNameRouter(interactor: interactor, viewController: viewController)
    }
    
    func build(listener: FormListener?) -> ViewableRouting {
        let viewController = InputNameViewController(themeStream: dependency.themeStream, keyboardObserver: dependency.keyboardObserver, mode: .update)
        let interactor = InputNameInteractor(presenter: viewController, authenticationService: dependency.authenticationService, permissionsManager: dependency.permissionsManager, userService: dependency.userService)
        interactor.listener = listener
        return InputNameRouter(interactor: interactor, viewController: viewController)
    }
    
    override init(dependency: InputNameDependency) {
        super.init(dependency: dependency)
    }
}
