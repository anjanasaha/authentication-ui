//
//  InputNameInteractor.swift
//   2020
//

import RIBs
import RxSwift
import UserInterface
import AuthenticationCommon
import CommonLib

protocol InputNameRouting: ViewableRouting { }

protocol InputNamePresentable: Presentable {
    var listener: InputNamePresentableListener? { get set }
    var displayName: String { get set }
    var profilePicture: UIImage? { get set }

    func dismissKeyboard()
    func present(error: Error)
    
    func launchCamera()
    func launchPhotoLibrary()

    func startLoadingAnimation()
    func stopLoadingAnimation()
    
    func changesSuccessful()
}

protocol InputNameListener: FormListener {
    func doneEditingDisplayName()
}

final class InputNameInteractor: PresentableInteractor<InputNamePresentable>, InputNameInteractable, InputNamePresentableListener {

    weak var router: InputNameRouting?
    weak var listener: FormListener?
    
    private let regex = "[a-zA-Z0-9_.]*"
    private let authenticationService: AuthenticationServicing
    private var uname: String?
    private var displayName: String?
    private var profilePicture: UIImage?
    private let userService: UserServicing
    private let permissionsManager: PermissionManaging
    
    init(presenter: InputNamePresentable, authenticationService: AuthenticationServicing, permissionsManager: PermissionManaging, userService: UserServicing) {
        self.authenticationService = authenticationService
        self.permissionsManager = permissionsManager
        self.userService = userService
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        
        authenticationService.authenticationStatus.subscribe(onNext: { [weak self] (status) in
            
            
            switch status {
                case .loggedIn(let result):
                    switch result {
                    case .success(let user):
                        self?.presenter.displayName = user.displayName ?? ""
                        self?.fetchImage()
                    case .failure(let error):
                        print(error)
                    }

                default:
                    break
            }
        }, onError: { (err) in
            print(err)
        }).disposeOnDeactivate(interactor: self)

        authenticationService.checkLogin()
    }
    
    func didEnter( username: String?) {
        self.uname = username
        
        if !(uname?.matches(regex: regex) ?? false) {
            print("Invalid : \(uname ?? "")")
        }
    }

    func didEnter( displayName: String?) {
        self.displayName = displayName
        
        if !(displayName?.matches(regex: regex) ?? false) {
            print("Invalid : \(displayName ?? "")")
        }
    }
    
    func removeProfilePicture() {
        
        userService.update(profile: .profilePhoto(.delete)).subscribe(onNext: { [weak self] in
            self?.profilePicture = nil
            self?.presenter.profilePicture = nil
        }, onError: { [weak self] (error) in
            self?.presenter.profilePicture = self?.profilePicture
        }).disposeOnDeactivate(interactor: self)
    }
    
    func didRequestToLaunchCamera() {
        
        permissionsManager.cameraPermissions.subscribe(onNext: { [weak self] (status) in
            switch status {
            case .allowed :
                DispatchQueue.main.async {
                    self?.presenter.launchCamera()
                }
            case .unknown :
                self?.permissionsManager.requestAccessCamera()
            default:
                break //self?.presenter.presentNoCameraAccess()
            }
        }).disposeOnDeactivate(interactor: self)
    }
    
    func didRequestToLaunchPhotoLibrary() {
        
        permissionsManager.photoLibraryPermissions.subscribe(onNext: { [weak self] (status) in
            switch status {
            case .allowed :
                self?.presenter.launchPhotoLibrary()
            case .unknown :
                self?.permissionsManager.requestAccessPhotoLibrary()
            default:
                break //self?.presenter.presentNoPhotoLibraryAccess()
            }
        }).disposeOnDeactivate(interactor: self)
    }
    
    func isPermitted(displayName: String, replacementString: String) -> Bool {
        return (displayName.count >= 24 ? isBackspace(replacementString) : (replacementString.rangeOfCharacter(from: CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._")) != nil || isBackspace(replacementString)) )
    }
    
    func didTapClose() {
        listener?.didTapBack(popped: true)
    }
    
    func didRequestToProceed() {
        
        presenter.startLoadingAnimation()

        // backendService.save(displayName: self.displayName, username: self.displayName)
        
        userService.update(profile: .name(.update(displayName ?? "")))
            .delay(.seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (success) in
            self?.presenter.stopLoadingAnimation()
            self?.presenter.changesSuccessful()
        }, onError: { [weak self] (error) in
            self?.presenter.stopLoadingAnimation()
        }).disposeOnDeactivate(interactor: self)
    }
    
    func didPickProfilePicture(_ image: UIImage) {
        // guard let data = image.jpegData(compressionQuality: 0.7) else { return }
        
        //backendService.uploadProfileImage(data)
        
        userService.update(profile: .profilePhoto(.update(image, compression: .compress(value: 0.7) ))).subscribe(onNext: { [weak self] url in
            self?.presenter.profilePicture = image
        }, onError: { error in
            print(error)
        }).disposeOnDeactivate(interactor: self)
    }

    private func fetchImage() {
        
        userService.fetchProfilePhoto(size: .thumbnail).subscribe(onNext: { [weak self]  (result) in
            guard let self = self else { return }
            switch result {
            case .success(let image):
                self.profilePicture = image
                self.presenter.profilePicture = image
            case .failure(_):
                self.presenter.profilePicture = nil
            }
            }, onError: { [weak self] error in
                guard let self = self else { return }
                self.presenter.profilePicture = nil
        }).disposeOnDeactivate(interactor: self)
    }
}
