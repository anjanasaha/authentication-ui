//
//  InputNameViewController.swift
//   2020
//

import RIBs
import RxSwift
import UIKit
import UserInterface
import Lottie
import CommonLib

protocol InputNamePresentableListener: class {
    func didPickProfilePicture(_ image: UIImage)
    func didEnter( username: String?)
    func didEnter( displayName: String?)
    func didTapClose()
    func didRequestToProceed()
    func didRequestToLaunchCamera()
    func didRequestToLaunchPhotoLibrary()
    func isPermitted(displayName: String, replacementString: String) -> Bool
    func removeProfilePicture()
}

final class InputNameViewController: BaseSettingsViewController, InputNamePresentable, InputNameViewControllable, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {

    weak var listener: InputNamePresentableListener?

    private let animationView: AnimationView

    private let usernameLength = 5
    private let imageView: ImageView
    private var enterUserNameTextField: TextField
    private var feedbackLabel: Label
    private let nextButton: LoadingButton
    private let mode: EditingMode
    private let tap = TapGestureRecognizer()
    
    var displayName: String = "" {
        didSet{
            enterUserNameTextField.text = "@\(displayName)"
        }
    }

    var profilePicture: UIImage? {
        didSet {
            guard let image = profilePicture else {
                showAnimation()
                return
            }

            showProfilePicture(image)
        }
    }
    
    init(themeStream: ThemeStreaming, keyboardObserver: KeyboardObserving, mode: EditingMode) {
        self.mode = mode
        self.imageView = ImageView(themeStream: themeStream, shadow: true)
        self.enterUserNameTextField = TextField(themeStream: themeStream, style: .regularPrimaryBold)
        self.feedbackLabel = Label(themeStream: themeStream, style: .smallSecondary)
        self.nextButton = LoadingButton(themeStream: themeStream, style: .regularPillSuccess)
        
        let addProfilePhotoImageAnimation: Animation? = AnimationLoader.load(source: .mainApp, name: "add_image")
        self.animationView = AnimationView(animation: addProfilePhotoImageAnimation)
        
        super.init(themeStream: themeStream, keyboardObserver: keyboardObserver)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = InputNameStrings.title
    }

    override func construct() {
        super.construct()
        
        tap.addTarget(self, action: #selector(addImageTapped(_:)))
        
        animationView.contentMode = .scaleAspectFit
        animationView.addGestureRecognizer(tap)

        view.addSubview(enterUserNameTextField)
        view.addSubview(feedbackLabel)
        view.addSubview(nextButton)
        view.addSubview(imageView)
        view.addSubview(animationView)
        
        enterUserNameTextField.addTarget(self, action: #selector(editingChanged(_:)), for: .editingChanged)
        enterUserNameTextField.addTarget(self, action: #selector(editingDidBegin(_:)), for: .editingDidBegin)
        enterUserNameTextField.placeholder = InputNameStrings.username.placeholder
        enterUserNameTextField.autocapitalizationType = .none
        enterUserNameTextField.delegate = self


        feedbackLabel.text = ((mode == .update) ? InputNameStrings.feedback.update : InputNameStrings.feedback.create )
        feedbackLabel.numberOfLines = 10
        
        nextButton.button.addTarget(self, action: #selector(submit(_:)), for: .touchUpInside)
        nextButton.button.isEnabled = false
        
        enterUserNameTextField.delegate = self

        switch mode {
        case .create:
            nextButton.button.setTitle( InputNameStrings.next, for: .normal)
        case .update:
            nextButton.button.setTitle( InputNameStrings.update, for: .normal)
        }

        showAnimation()
    }
    
    @objc func addImageTapped(_ sender: Button) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: InputNameStrings.alert.option1, style: .default) { [weak self] (action) in
            self?.listener?.didRequestToLaunchCamera()
        }
        
        let photoLibraryAction = UIAlertAction(title: InputNameStrings.alert.option2, style: .default) { [weak self] (action) in
            self?.listener?.didRequestToLaunchPhotoLibrary()
        }
        
        let removeImageAction = UIAlertAction(title: InputNameStrings.alert.removePicture, style: .destructive) { [weak self] (action) in
            self?.profilePicture = nil
            self?.listener?.removeProfilePicture()
        }

        let cancelAction = UIAlertAction(title: InputNameStrings.alert.cancel, style: .cancel)

        alert.addAction(cameraAction)
        alert.addAction(photoLibraryAction)
        
        if profilePicture != nil {
            alert.addAction(removeImageAction)
        }
        
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }

    override func layout() {
        super.layout()

        animationView.snp.makeConstraints {
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(2.dip)
            $0.top.equalTo(view.safeAreaLayoutGuide).offset(3.dip)
            $0.height.width.equalTo(10.dip)
        }

        imageView.snp.makeConstraints {
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(2.dip)
            $0.top.equalTo(view.safeAreaLayoutGuide).offset(3.dip)
            $0.height.width.equalTo(10.dip)
        }

        enterUserNameTextField.snp.makeConstraints {
            $0.leading.equalTo(imageView.snp.trailing).offset(leadingGutter)
            $0.bottom.equalTo(imageView.snp.centerY)
            $0.height.greaterThanOrEqualTo(3.dip)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
        }
        
        feedbackLabel.snp.makeConstraints {
            $0.leading.equalTo(imageView.snp.trailing).offset(leadingGutter)
            $0.top.equalTo(enterUserNameTextField.snp.bottom).offset(topGutter * 0.25)
            $0.height.greaterThanOrEqualTo(3.dip)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter * 0.85)
        }
        
        nextButton.snp.makeConstraints {
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.bottom.equalTo(view.safeAreaLayoutGuide).offset(bottomGutter/4)
            $0.height.equalTo(nextButton.button.height)
            $0.width.equalTo(nextButton.button.intrinsicContentSize.width)
        }

    }

    func changesSuccessful() {
        nextButton.button.isEnabled = false
    }

    override func keyboardStatus(event: KeyboardEvent, height: CGFloat) {
        
        guard event == .opened || event == .closed else { return }
        
        nextButton.snp.removeConstraints()

        var computedHeight: CGFloat = 0.0
        
        if (event == .opened) {
            computedHeight = -height + gap
        } else if (event == .closed) {
            computedHeight = -gap
        }

        nextButton.snp.makeConstraints {
            $0.bottom.equalTo(view.safeAreaLayoutGuide).offset(computedHeight)
            $0.trailing.equalTo(view.safeAreaLayoutGuide).inset(leadingGutter)
            $0.height.equalTo(nextButton.button.height)
            $0.width.equalTo(nextButton.button.intrinsicContentSize.width)
        }
        
        view.setNeedsUpdateConstraints()
        
        UIView.animate(withDuration: 0.35, animations: { [weak self] () -> Void in
            self?.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    override func viewDidLayoutSubviews() {

        nextButton.layer.cornerRadius = nextButton.frame.height / 2
        imageView.layer.cornerRadius = imageView.frame.height / 2
        imageView.clipsToBounds = true
        nextButton.clipsToBounds = true
        nextButton.button.themeApply()
    }

    @objc private func didTapClose() {
        resignFirstResponder()
        listener?.didTapClose()
    }
    
    @objc private func editingChanged(_ textfield: UITextField) {
        
        if textfield == enterUserNameTextField, let text = textfield.text {

            if !text.starts(with: "@") {
                textfield.text = "@\(text)"
            }

            var offsetBy = 0
            
            if text.count > 0 {
                offsetBy = -(text.count - 1)
            }

            let index = text.index(text.endIndex, offsetBy: offsetBy)
            let name = text.suffix(from: index).trimmingCharacters(in: .whitespacesAndNewlines)
            
            nextButton.button.isEnabled = (name.count > usernameLength)

            listener?.didEnter(displayName: name)
        }

    }
    
    @objc private func submit(_ sender: Button) {
        listener?.didRequestToProceed()
    }
    
    func startLoadingAnimation() {
        nextButton.play()
    }

    func stopLoadingAnimation() {
        nextButton.stop()
    }
    
    @objc private func editingDidBegin(_ textfield: UITextField) {
        
        if textfield == enterUserNameTextField {
            let text = textfield.text ?? ""
            
            if !text.starts(with: "@") {
                textfield.text = "@\(text)"
            }
        }
    }
    
    func showAnimation() {
        animationView.isHidden = false
        animationView.addGestureRecognizer(tap)
        imageView.removeGestureRecognizer(tap)
        imageView.isHidden = true
        animationView.play(fromProgress: 0, toProgress: 1, loopMode: .loop, completion: nil)
    }
    
    func showProfilePicture(_ image: UIImage?) {
        animationView.isHidden = true
        imageView.image = image
        animationView.removeGestureRecognizer(tap)
        imageView.addGestureRecognizer(tap)
        imageView.isHidden = false
    }
    
    /// MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else { return }
        imageView.image = image
        listener?.didPickProfilePicture(image)
    }
    
    func dismissKeyboard() {
        enterUserNameTextField.resignFirstResponder()
    }
    
    func present(error: Error) {
        if let err = error as? ErrorReason {
            feedbackLabel.style = .smallWarning
            feedbackLabel.text = err.readable
        }
    }
    
    func launchCamera() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.cameraDevice = .front
        vc.cameraFlashMode = .off
        vc.cameraCaptureMode = .photo
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    
    func launchPhotoLibrary() {
        
    }
}

extension InputNameViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == enterUserNameTextField, let text = textField.text {
            return listener?.isPermitted(displayName: text, replacementString: string) ?? false
        }

        return true
    }
}
