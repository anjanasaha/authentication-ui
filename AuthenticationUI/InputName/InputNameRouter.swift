//
//  InputNameRouter.swift
//   2020
//

import RIBs
import UserInterface

protocol InputNameInteractable: Interactable {
    var router: InputNameRouting? { get set }
    var listener: FormListener? { get set }
}

protocol InputNameViewControllable: ViewControllable { }

final class InputNameRouter: ViewableRouter<InputNameInteractable, InputNameViewControllable>, InputNameRouting {

    override init(interactor: InputNameInteractable, viewController: InputNameViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
