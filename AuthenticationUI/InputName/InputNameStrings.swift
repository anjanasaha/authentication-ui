//
//  InputNameStrings.swift
//  2020
//

import CommonLib

final class InputNameStrings: StringProvider {
    
    /* Display name */
    static var title: String {
        return StringLoader.load(forKey: "\(RIBName).title")
    }
    
    /* Enter username */
    struct username {
        static var placeholder: String {
            return StringLoader.load(forKey: "\(RIBName).username.placeholder")
        }
    }

    /* Enter display name */
    struct displayName {
        static var placeholder: String {
            return StringLoader.load(forKey: "\(RIBName).displayName.placeholder")
        }
    }
    
    /* Enter Alert title */
    struct alert {
        static var title: String {
            return StringLoader.load(forKey: "\(RIBName).alert.title")
        }
        
        static var option1: String {
            return StringLoader.load(forKey: "\(RIBName).alert.option1")
        }
        
        static var option2: String {
            return StringLoader.load(forKey: "\(RIBName).alert.option2")
        }
        
        static var cancel: String {
            return StringLoader.load(forKey: "\(RIBName).alert.cancel")
        }
        
        static var removePicture: String {
            return StringLoader.load(forKey: "\(RIBName).alert.removePicture")
        }
    }
    
    /* This name will be visible to your contacts. */
    struct feedback {
        
        static var create: String {
            return StringLoader.load(forKey: "\(RIBName).feedback.create")
        }
        
        static var update: String {
            return StringLoader.load(forKey: "\(RIBName).feedback.update")
        }
    }
    
    /* Next */
    static var next: String {
        return StringLoader.load(forKey: "\(RIBName).next")
    }
    
    /* Update */
    static var update: String {
        return StringLoader.load(forKey: "\(RIBName).update")
    }
}
