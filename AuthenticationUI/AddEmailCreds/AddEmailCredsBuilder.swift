//
//  AddEmailCredsBuilder.swift
//  Authentication
//

import RIBs
import UserInterface
import AuthenticationCommon

public protocol AddEmailCredsDependency: AuthenticationCoreDependency { }

final class AddEmailCredsComponent: Component<AddEmailCredsDependency> { }

// MARK: - Builder

protocol AddEmailCredsBuildable: Buildable {
    func build(withListener listener: AddEmailCredsListener?, mode: EditingMode) -> AddEmailCredsRouting
    func build(withListener listener: AddEmailCredsListener?) -> AddEmailCredsRouting
}

final class AddEmailCredsBuilder: Builder<AddEmailCredsDependency>, AddEmailCredsBuildable {

    override init(dependency: AddEmailCredsDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: AddEmailCredsListener?, mode: EditingMode) -> AddEmailCredsRouting {
        let viewController = AddEmailCredsViewController(themeStream: dependency.themeStream, keyboardObserver: dependency.keyboardObserver, mode: mode)
        let interactor = AddEmailCredsInteractor(presenter: viewController, authenticationService: dependency.authenticationService, keyboardObserver: dependency.keyboardObserver, mode: mode)
        interactor.listener = listener
        return AddEmailCredsRouter(interactor: interactor, viewController: viewController)
    }
    
    func build(withListener listener: AddEmailCredsListener?) -> AddEmailCredsRouting {
        return build(withListener: listener, mode: .create)
    }
}
