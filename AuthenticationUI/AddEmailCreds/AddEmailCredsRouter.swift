//
//  AddEmailCredsRouter.swift
//  Authentication
//

import RIBs

protocol AddEmailCredsInteractable: Interactable {
    var router: AddEmailCredsRouting? { get set }
    var listener: AddEmailCredsListener? { get set }
}

protocol AddEmailCredsViewControllable: ViewControllable, UITextFieldDelegate { }

final class AddEmailCredsRouter: ViewableRouter<AddEmailCredsInteractable, AddEmailCredsViewControllable>, AddEmailCredsRouting {

    override init(interactor: AddEmailCredsInteractable, viewController: AddEmailCredsViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    func cleanUpViews() {
        guard let navigationController = viewControllable.uiviewController.navigationController else {
            return
        }
        
        navigationController.popViewController(animated: false)
    }
}
