//
//  AddEmailCredsInteractor.swift
//  Authentication
//

import RIBs
import RxSwift
import UserInterface
import AuthenticationCommon

protocol AddEmailCredsRouting: ViewableRouting {
    func cleanUpViews()
}

protocol AddEmailCredsPresentable: Presentable {
    var listener: AddEmailCredsPresentableListener? { get set }
    var pageTitle: String { get set }

    func presentError(error: AddEmailError)
    func fill(email: String)
    func passwordsMatch()
    func clearError()
}

public protocol AddEmailCredsListener: class {
    func addingEmailPasswordDidSucceed()
}

final class AddEmailCredsInteractor: PresentableInteractor<AddEmailCredsPresentable>, AddEmailCredsInteractable, AddEmailCredsPresentableListener {

    weak var router: AddEmailCredsRouting?
    weak var listener: AddEmailCredsListener?

    private let authenticationService: AuthenticationServicing
    private let mode: EditingMode
    
    private var email = ""
    private var password = ""
    private var confirmPassword = ""
    
    init(presenter: AddEmailCredsPresentable, authenticationService: AuthenticationServicing, keyboardObserver: KeyboardObserving, mode: EditingMode = .create) {
        self.authenticationService = authenticationService
        self.mode = mode
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        
        presenter.pageTitle = (mode == .create ? AddEmailCredsStrings.title.create : AddEmailCredsStrings.title.update)
    }

    override func willResignActive() {
        super.willResignActive()
        
        router?.cleanUpViews()
    }
    
    // MARK: AddNamePresentableListener
    func didTapNext() {
        
        if password.isEmpty {
            presenter.presentError(error: .passwordEmpty)
            return
        }

        if isPasswordAcceptable() && confirmPassword.isEmpty {
            presenter.presentError(error: .confirmPasswordEmpty)
            return
        }
        
        if isPasswordAcceptable() && password != confirmPassword {
            presenter.presentError(error: .confirmPasswordDoesntMatch)
            return
        }

        // Link account with email
        authenticationService.link(email: email, password: password).subscribe(onNext: { [weak self] (status) in
            self?.listener?.addingEmailPasswordDidSucceed()
        }, onError: { [weak self] (error) in
            guard let self = self else { return }
            self.authenticationService.signOut().subscribe().disposeOnDeactivate(interactor: self)
        }).disposeOnDeactivate(interactor: self)

    }

    func didEnter(email: String) {
        self.email = email
    }
    
    func didEnter(password: String) {
        self.password = password
        presenter.clearError()
    }
    
    func didEnter(confirmPassword: String) {
        self.confirmPassword = confirmPassword
        
        if !password.contains(confirmPassword) {
            presenter.presentError(error: .passwordMismatch)
        } else if (password.count == confirmPassword.count && password == confirmPassword) {
            presenter.passwordsMatch()
        }
    }
    
    func passwordConfrimed() -> Bool {
        return password == confirmPassword
    }

    func isPasswordAcceptable() -> Bool {

        if !password.isEmpty {
            return true
        }
        
        return false
    }
}

enum AddEmailError {
    case confirmPasswordDoesntMatch
    case confirmPasswordEmpty
    case none
    case other(String)
    case passwordMismatch
    case passwordEmpty
}

enum EditingMode {
    case create, update
}
