//
//  AddEmailCredsStrings.swift
//  Authentication
//

import Foundation
import CommonLib

final class AddEmailCredsStrings: StringProvider {
    
    static var emailPlaceholderText: String {
        return StringLoader.load(forKey: "Email")
    }
    
    static var passwordPlaceholderText: String {
        return StringLoader.load(forKey: "Password")
    }
    
    static var confirmPasswordPlaceholderText: String {
        return StringLoader.load(forKey: "Confirm password")
    }
    
    static var save: String {
        return StringLoader.load(forKey: "\(RIBName).save")
    }
    
    static var purposeOfPassword: String {
        return StringLoader.load(forKey: "\(RIBName).purposeOfPassword")
    }

    struct title {
        static var create: String {
            return StringLoader.load(forKey: "\(RIBName).title.create")
        }
        
        static var update: String {
            return StringLoader.load(forKey: "\(RIBName).title.update")
        }
    }
}
