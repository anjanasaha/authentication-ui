//
//  AuthenticationUIDependency.swift
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved. 2020
//

import UserInterface
import AuthenticationCommon
import RIBs

public protocol AuthenticationCoreDependency: Dependency {
    var themeStream: ThemeStreaming { get }
    var keyboardObserver: KeyboardObserving { get }
    var authenticationService: AuthenticationServicing { get }
    var permissionsManager: PermissionManaging { get }
    var userService: UserServicing { get }
}
