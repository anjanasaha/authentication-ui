//
//  PhoneNumberSignInBuilder.swift
//

import RIBs
import UserInterface
import AuthenticationCommon

public protocol PhoneNumberSignInDependency: PhoneNumberVerificationDependency {
    var themeStream: ThemeStreaming { get }
    var backendService: AuthenticationServicing { get }
    var keyboardEventObserver: KeyboardObserving { get }
}

final class PhoneNumberSignInComponent: Component<PhoneNumberSignInDependency> {

    fileprivate var phoneNumberVerificationBuilder: PhoneNumberVerificationBuildable {
        return PhoneNumberVerificationBuilder(dependency: dependency)
    }
    
    fileprivate var backendService: AuthenticationServicing {
        return dependency.backendService
    }
}

// MARK: - Builder

public protocol PhoneNumberSignInBuildable: Buildable {
    func build(withListener listener: PhoneNumberSignInListener) -> PhoneNumberSignInRouting
}

public final class PhoneNumberSignInBuilder: Builder<PhoneNumberSignInDependency>, PhoneNumberSignInBuildable {

    override init(dependency: PhoneNumberSignInDependency) {
        super.init(dependency: dependency)
    }

    public func build(withListener listener: PhoneNumberSignInListener) -> PhoneNumberSignInRouting {
        let component = PhoneNumberSignInComponent(dependency: dependency)
        let viewController = PhoneNumberSignInViewController(themeStream: dependency.themeStream)
        let interactor = PhoneNumberSignInInteractor(presenter: viewController, backendService: component.backendService, keyboardObserver: dependency.keyboardEventObserver)
        interactor.listener = listener
        return PhoneNumberSignInRouter(interactor: interactor, viewController: viewController, verifyNumberBuilder: component.phoneNumberVerificationBuilder)
    }
}
