//
//  PhoneNumberSignInInteractor.swift
//  Authentication
//

import RIBs
import UserInterface
import RxSwift
import AuthenticationCommon

public protocol PhoneNumberSignInRouting: ViewableRouting {
    func routeToVerifyNumber(phoneNumber: String)
    func routeAwayFromVerifyNumber()
}

protocol PhoneNumberSignInPresentable: Presentable {
    var listener: PhoneNumberSignInPresentableListener? { get set }
    
    func show(alert: AlertViewModelling)
}

public protocol PhoneNumberSignInListener: class {
    func loginSuccess()
}

final class PhoneNumberSignInInteractor: PresentableInteractor<PhoneNumberSignInPresentable>, PhoneNumberSignInInteractable, PhoneNumberSignInPresentableListener {
    
    private let backendService: AuthenticationServicing
    private var keyboardObserver: KeyboardObserving

    weak var router: PhoneNumberSignInRouting?
    weak var listener: PhoneNumberSignInListener?

    init(presenter: PhoneNumberSignInPresentable, backendService: AuthenticationServicing, keyboardObserver: KeyboardObserving) {
        self.keyboardObserver = keyboardObserver
        self.backendService = backendService
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    func didEnter(phoneNumber: String, countryCode: CountryCode) {
        let countryCodePhoneNumber = "\(countryCode.rawValue)\(phoneNumber)"
        backendService.verify(phoneNumber: countryCodePhoneNumber).subscribe(onNext: { [weak self] (verificationString) in
            guard let self = self else { return }
            self.router?.routeToVerifyNumber(phoneNumber: verificationString)
        }, onError: { [weak self]  error in
            self?.presenter.show(alert: AlertViewModel(alertViewType: .error, title: "Authorization failed", subtitle: error.localizedDescription, icon: nil) )
        }).disposeOnDeactivate(interactor: self)
    }
    
    func loginRequestSuccess(data: Bool) {
        listener?.loginSuccess()
    }
    
    func loginRequestFailed(error: Error) {
        router?.routeAwayFromVerifyNumber()
    }
    
}
