//
//  PhoneNumberSignInStrings.swift
//  2020
//

import CommonLib

final class PhoneNumberSignInStrings: StringProvider {

    static var submitButtonTitle: String {
        return StringLoader.load(forKey: "\(RIBName).submit.title")
    }
    
    static func resendInTime(_ parameter: String) -> String {
        return "\(StringLoader.load(forKey: "\(RIBName).resendInTime.title")) \(parameter)s."
    }
    
    static func resendInTime() -> String {
        return "\(StringLoader.load(forKey: "\(RIBName).resend.title"))"
    }
}
