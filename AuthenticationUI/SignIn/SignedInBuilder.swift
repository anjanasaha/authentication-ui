//
//  SignedInBuilder.swift
//  Authentication
//
//

import RIBs
import UserInterface

public protocol SignedInDependency: AuthenticationCoreDependency { }

final class SignedInComponent: Component<SignedInDependency> {
}

// MARK: - Builder

public protocol SignedInBuildable: Buildable {
    func build(withListener listener: SignedInListener) -> SignedInRouting
}

public final class DefaultSignedInBuilder: Builder<SignedInDependency>, SignedInBuildable {

    public override init(dependency: SignedInDependency) {
        super.init(dependency: dependency)
    }

    public func build(withListener listener: SignedInListener) -> SignedInRouting {
        _ = SignedInComponent(dependency: dependency)
        let viewController = SignedInViewController()
        let interactor = SignedInInteractor(presenter: viewController)
        interactor.listener = listener
        return SignedInRouter(interactor: interactor, viewController: viewController)
    }
}
