//
//  SignedInRouter.swift
//  Authentication
//

import RIBs

public protocol SignedInInteractable: Interactable {
    var router: SignedInRouting? { get set }
    var listener: SignedInListener? { get set }
}

public protocol SignedInViewControllable: ViewControllable {
    // TODO: Declare methods the router invokes to manipulate the view hierarchy.
}

final class SignedInRouter: ViewableRouter<SignedInInteractable, SignedInViewControllable>, SignedInRouting {

    // TODO: Constructor inject child builder protocols to allow building children.
    override init(interactor: SignedInInteractable, viewController: SignedInViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
