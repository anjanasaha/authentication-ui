//
//  SignedInInteractor.swift
//  Authentication
//

import RIBs
import RxSwift

public protocol SignedInRouting: ViewableRouting {
    // TODO: Declare methods the interactor can invoke to manage sub-tree via the router.
}

public protocol SignedInPresentable: Presentable {
    var listener: SignedInPresentableListener? { get set }
    // TODO: Declare methods the interactor can invoke the presenter to present data.
}

public protocol SignedInListener: class {
    // TODO: Declare methods the interactor can invoke to communicate with other RIBs.
}

final class SignedInInteractor: PresentableInteractor<SignedInPresentable>, SignedInInteractable, SignedInPresentableListener {

    weak var router: SignedInRouting?
    weak var listener: SignedInListener?

    // TODO: Add additional dependencies to constructor. Do not perform any logic
    // in constructor.
    override init(presenter: SignedInPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        // TODO: Implement business logic here.
    }

    override func willResignActive() {
        super.willResignActive()
        // TODO: Pause any business logic.
    }
}
