//
//  RegistrationFlowInteractor.swift
//   2020
//

import RIBs
import RxSwift
import AuthenticationCommon

public protocol RegistrationFlowRouting: Routing {
    func routeToAddDisplayName()
    func routeToAddEmail()
    func routeAwayFromProfile()
    func routeAwayFromAddEmail()
}

public protocol RegistrationFlowListener: class {
    func registrationFlowComplete()
}

final class RegistrationFlowInteractor: Interactor, RegistrationFlowInteractable {

    weak var router: RegistrationFlowRouting?
    weak var listener: RegistrationFlowListener?

    var authenticationService: AuthenticationServicing

    private var user: UserEntity
    
    init(authenticationService: AuthenticationServicing, user: UserEntity) {
        self.user = user
        self.authenticationService = authenticationService
        super.init()
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        
        if !user.hasEmail {
            router?.routeToAddEmail()
        } else if !user.hasDisplayName {
            router?.routeToAddDisplayName()
        }
    }
    
    func didTapBack(popped: Bool) {
        router?.routeAwayFromProfile()
        listener?.registrationFlowComplete()
    }
    
    
    func addingEmailPasswordDidSucceed() {
        router?.routeAwayFromAddEmail()
        router?.routeToAddDisplayName()
    }
    
}
