//
//  RegistrationFlowBuilder.swift
//   2020
//

import RIBs
import UserInterface
import AuthenticationCommon

public protocol RegistrationFlowDependency: AddEmailCredsDependency, InputNameDependency { }

public final class RegistrationFlowComponent: Component<RegistrationFlowDependency> {

    fileprivate var addEmailCredsBuilder: AddEmailCredsBuildable {
        return shared { AddEmailCredsBuilder(dependency: dependency) }
    }
    
    fileprivate var inputNameBuilder: InputNameBuildable {
        return shared { InputNameBuilder(dependency: dependency) }
    }
}

// MARK: - Builder

public protocol RegistrationFlowBuildable: Buildable {
    func build(withListener listener: RegistrationFlowListener, user: UserEntity, registrationFlowViewController: RegistrationFlowViewControllable) -> RegistrationFlowRouting
}

public final class DefaultRegistrationFlowBuilder: Builder<RegistrationFlowDependency>, RegistrationFlowBuildable {

    public override init(dependency: RegistrationFlowDependency) {
        super.init(dependency: dependency)
    }

    public func build(withListener listener: RegistrationFlowListener, user: UserEntity, registrationFlowViewController: RegistrationFlowViewControllable) -> RegistrationFlowRouting {
        let component = RegistrationFlowComponent(dependency: dependency)
        let interactor = RegistrationFlowInteractor(authenticationService: dependency.authenticationService, user: user)
        interactor.listener = listener
        return RegistrationFlowRouter(interactor: interactor, viewController: registrationFlowViewController, addEmailCredsBuilder: component.addEmailCredsBuilder, inputNameBuilder: component.inputNameBuilder, themeStream: dependency.themeStream)
    }
}
