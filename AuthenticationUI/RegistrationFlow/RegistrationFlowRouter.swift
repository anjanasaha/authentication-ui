//
//  RegistrationFlowRouter.swift
//   2020
//

import RIBs
import UserInterface

public protocol RegistrationFlowInteractable: Interactable, AddEmailCredsListener, FormListener {
    var router: RegistrationFlowRouting? { get set }
    var listener: RegistrationFlowListener? { get set }
}

public protocol RegistrationFlowViewControllable: ViewControllable {
    func presentVC(viewController: UIViewController)
    func dismissVC()
}

final class RegistrationFlowRouter: Router<RegistrationFlowInteractable>, RegistrationFlowRouting {

    private var viewController: RegistrationFlowViewControllable

    private var navigation: NavigationController?
    private let inputNameBuilder: InputNameBuildable
    weak var inputNameRouter: InputNameRouting?
    
    private var themeStream: ThemeStreaming
    private let addEmailCredsBuilder: AddEmailCredsBuildable
    weak var addEmailCredsRouter: AddEmailCredsRouting?


    init(interactor: RegistrationFlowInteractable, viewController: RegistrationFlowViewControllable, addEmailCredsBuilder: AddEmailCredsBuildable, inputNameBuilder: InputNameBuildable, themeStream: ThemeStreaming) {
        self.themeStream = themeStream
        self.addEmailCredsBuilder = addEmailCredsBuilder
        self.inputNameBuilder = inputNameBuilder
        self.viewController = viewController
        super.init(interactor: interactor)
        interactor.router = self
    }

    func routeToAddDisplayName() {
        guard inputNameRouter == nil else { return }
        let theRouter = inputNameBuilder.build(withListener: interactor, mode: .create)
        attachChild(theRouter)
        inputNameRouter = theRouter
        
        let nav = NavigationController(rootViewController: theRouter.viewControllable.uiviewController, themeStream: themeStream)
        navigation = nav
        viewController.presentVC(viewController: nav)
    }
    
    func routeAwayFromProfile() {
        guard let theRouter = inputNameRouter else { return }
        detachChild(theRouter)
        inputNameRouter = nil
        viewController.dismissVC()
    }
    
    func routeToAddEmail() {
        guard addEmailCredsRouter == nil else { return }
        let theRouter = addEmailCredsBuilder.build(withListener: interactor)
        attachChild(theRouter)
        addEmailCredsRouter = theRouter

        let nav = NavigationController(rootViewController: theRouter.viewControllable.uiviewController, themeStream: themeStream)
        navigation = nav
        viewController.presentVC(viewController: nav)
    }

    func routeAwayFromAddEmail() {
        guard let theRouter = addEmailCredsRouter else { return }
        detachChild(theRouter)
        addEmailCredsRouter = nil
        viewController.dismissVC()
    }
}
