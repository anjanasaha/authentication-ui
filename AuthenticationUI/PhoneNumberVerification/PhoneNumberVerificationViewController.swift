//
//  PhoneNumberVerificationViewController.swift
//  Hopshop
//

import RIBs
import RxSwift
import UserInterface
import CommonLib

protocol PhoneNumberVerificationPresentableListener: class {
    func didEnter(verificationCode: String)
}

final class PhoneNumberVerificationViewController: ViewController, PhoneNumberVerificationPresentable, PhoneNumberVerificationViewControllable {

    weak var listener: PhoneNumberVerificationPresentableListener?
    
    private var responderChains = [TextField]()
    
    private let resendButton: Button

    private var firstCharacterField: TextField!
    private var secondCharacterField: TextField!
    private var thirdCharacterField: TextField!
    private var forthCharacterField: TextField!
    private var fifthCharacterField: TextField!
    private var sixthCharacterField: TextField!
    
    private var firstCharacterHorizontalLine: HorizontalLine!
    private var secondCharacterHorizontalLine: HorizontalLine!
    private var thirdCharacterHorizontalLine: HorizontalLine!
    private var forthCharacterHorizontalLine: HorizontalLine!
    private var fifthCharacterHorizontalLine: HorizontalLine!
    private var sixthCharacterHorizontalLine: HorizontalLine!

    private let boxWidth = 5.dip
    private let boxHeight = 7.dip
    private let verifyCode = "Verify code"
    private let submitButton: Button

    override init(themeStream: ThemeStreaming, keyboardObserver: KeyboardObserving? = nil) {
        self.resendButton = Button(themeStream: themeStream, style: .smallPillTransparent)
        self.submitButton = Button(themeStream: themeStream, style: .regularPillSuccess)
        super.init(themeStream: themeStream, keyboardObserver: keyboardObserver)
        title = verifyCode
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        resendButton.isHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        resendButton.themeApply()
        submitButton.themeApply()
    }

    override func construct() {
        super.construct()

        firstCharacterField = TextField(themeStream: themeStream, style: .largePrimary)
        secondCharacterField = TextField(themeStream: themeStream, style: .largePrimary)
        thirdCharacterField = TextField(themeStream: themeStream, style: .largePrimary)
        forthCharacterField = TextField(themeStream: themeStream, style: .largePrimary)
        fifthCharacterField = TextField(themeStream: themeStream, style: .largePrimary)
        sixthCharacterField = TextField(themeStream: themeStream, style: .largePrimary)
        
        firstCharacterHorizontalLine = HorizontalLine(themeStream: themeStream)
        secondCharacterHorizontalLine = HorizontalLine(themeStream: themeStream)
        thirdCharacterHorizontalLine = HorizontalLine(themeStream: themeStream)
        forthCharacterHorizontalLine = HorizontalLine(themeStream: themeStream)
        fifthCharacterHorizontalLine = HorizontalLine(themeStream: themeStream)
        sixthCharacterHorizontalLine = HorizontalLine(themeStream: themeStream)

        build(textfield: firstCharacterField, line: firstCharacterHorizontalLine)
        build(textfield: secondCharacterField, line: secondCharacterHorizontalLine)
        build(textfield: thirdCharacterField, line: thirdCharacterHorizontalLine)
        build(textfield: forthCharacterField, line: forthCharacterHorizontalLine)
        build(textfield: fifthCharacterField, line: fifthCharacterHorizontalLine)
        build(textfield: sixthCharacterField, line: sixthCharacterHorizontalLine)

        
        submitButton.setTitle(PhoneNumberVerificationStrings.submitButtonTitle, for: .normal)
        submitButton.addTarget(self, action: #selector(verifyCode(_:)), for: .touchUpInside)

        view.addSubview(submitButton)
        
        view.addSubview(resendButton)
        resendButton.isHidden = true
    }

    @objc private func verifyCode(_ sender: Button) {
        guard let firstChar = firstCharacterField.text, let secondChar = secondCharacterField.text, let thirdChar = thirdCharacterField.text, let forthChar = forthCharacterField.text, let fifthChar = fifthCharacterField.text, let sixthChar = sixthCharacterField.text else {
            return
        }

        let verificationCode = "\(firstChar)\(secondChar)\(thirdChar)\(forthChar)\(fifthChar)\(sixthChar)"

        listener?.didEnter(verificationCode: verificationCode)
    }

    func presentResend(title: String, enabled: Bool) {
        resendButton.setTitle(title, for: .normal)
        resendButton.isEnabled = enabled
    }

    private func build(textfield: TextField, line: HorizontalLine) {
        responderChains.append(textfield)
        textfield.textAlignment = .center
        // line.backgroundColor = Color.gray60
        line.layer.cornerRadius = 2
        view.addSubview(line)
        view.addSubview(textfield)
        textfield.keyboardType = .decimalPad
        textfield.delegate = self
        textfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }

    override func layout() {
        super.layout()
        
        firstCharacterField.snp.makeConstraints { (maker) in
            maker.top.equalTo(20.dip)
            maker.leading.equalTo(view.safeAreaLayoutGuide).offset(1.5.dip)
            maker.width.equalTo(boxWidth)
            maker.height.equalTo(boxHeight)
        }
        firstCharacterHorizontalLine.snp.makeConstraints { (maker) in
            maker.leading.equalTo(firstCharacterField.snp.leadingMargin)
            maker.trailing.equalTo(firstCharacterField.snp.trailingMargin)
            maker.height.equalTo(0.5.dip)
            maker.top.equalTo(firstCharacterField.snp.bottomMargin).offset(0.5.dip)
        }
        
        setupConstraints(line: secondCharacterHorizontalLine, textfield: secondCharacterField, leftOf: firstCharacterField)
        setupConstraints(line: thirdCharacterHorizontalLine, textfield: thirdCharacterField, leftOf: secondCharacterField)
        setupConstraints(line: forthCharacterHorizontalLine, textfield: forthCharacterField, leftOf: thirdCharacterField)
        setupConstraints(line: fifthCharacterHorizontalLine, textfield: fifthCharacterField, leftOf: forthCharacterField)
        setupConstraints(line: sixthCharacterHorizontalLine, textfield: sixthCharacterField, leftOf: fifthCharacterField)

        resendButton.snp.makeConstraints {
            $0.leading.equalTo(firstCharacterHorizontalLine.snp.leading)
            $0.centerY.equalTo(submitButton.snp.centerY)
            $0.height.equalTo(resendButton.height)
        }

        submitButton.snp.makeConstraints {
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.bottom.equalTo(view.safeAreaLayoutGuide).offset(bottomGutter)
            $0.height.equalTo(submitButton.height)
        }
    }
    
    override func keyboardStatus(event: KeyboardEvent, height: CGFloat) {

        guard event == .opened || event == .closed else { return }
        
        submitButton.snp.removeConstraints()

        var computedHeight: CGFloat = 0.0
        
        if (event == .opened) {
            computedHeight = gap - height
        } else if (event == .closed) {
            computedHeight = -gap
        }
        
        resendButton.snp.makeConstraints {
            $0.leading.equalTo(firstCharacterHorizontalLine.snp.leading)
            $0.centerY.equalTo(submitButton.snp.centerY)
            $0.height.equalTo(4.dip)
        }
        submitButton.snp.makeConstraints {
            $0.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            $0.bottom.equalTo(view.safeAreaLayoutGuide).offset(computedHeight)
            $0.height.equalTo(5.dip)
        }
        
        view.setNeedsUpdateConstraints()
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    private func setupConstraints(line: HorizontalLine, textfield: TextField, leftOf: UIView) {
        textfield.snp.makeConstraints { (maker) in
            maker.leading.equalTo(leftOf.snp.trailingMargin).offset(1.5.dip)
            maker.centerY.equalTo(leftOf.safeAreaLayoutGuide)
            maker.width.equalTo(boxWidth)
            maker.height.equalTo(boxHeight)
        }
        line.snp.makeConstraints { (maker) in
            maker.leading.equalTo(textfield.snp.leadingMargin)
            maker.trailing.equalTo(textfield.snp.trailingMargin)
            maker.height.equalTo(0.5.dip)
            maker.top.equalTo(textfield.snp.bottomMargin).offset(0.5.dip)
        }
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {
        guard let text = textField.text, let textField = textField as? TextField else { return }
        guard let index = responderChains.firstIndex(of: textField) else { return }
        
        if index < responderChains.count - 1 {
            let nextTextField = responderChains[index + 1]
            if text.count != 0 {
                nextTextField.becomeFirstResponder()
            }
        }
    }
}

extension PhoneNumberVerificationViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textCount = textField.text?.count, textCount == 0, let ch = string.unicodeScalars.first else {
            return isBackspace(string)
        }
        let digits = CharacterSet.decimalDigits
        let isADigit = digits.contains(ch)
        return isADigit
    }
}
