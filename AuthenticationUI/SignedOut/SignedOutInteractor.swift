//
//  SignedOutInteractor.swift
//   2020
//

import RIBs
import RxSwift
import AuthenticationCommon
import UserInterface

public protocol SignedOutRouting: ViewableRouting {
    func routeToVerifyNumber(phoneNumber: String)
    func routeAwayFromVerifyNumber()
}

public protocol SignedOutPresentable: Presentable {
    var listener: SignedOutPresentableListener? { get set }
    
    func show(alert: AlertViewModelling)
}

public protocol SignedOutListener: class {
    func loginSuccess()
}

final class SignedOutInteractor: PresentableInteractor<SignedOutPresentable>, SignedOutInteractable, SignedOutPresentableListener {

    private let authenticationService: AuthenticationServicing
    private var keyboardObserver: KeyboardObserving
    
    weak var router: SignedOutRouting?
    weak var listener: SignedOutListener?

    init(presenter: SignedOutPresentable, authenticationService: AuthenticationServicing, keyboardObserver: KeyboardObserving) {
        self.keyboardObserver = keyboardObserver
        self.authenticationService = authenticationService
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    func didEnter(phoneNumber: String, countryCode: CountryCode) {
        let countryCodePhoneNumber = "\(countryCode.rawValue)\(phoneNumber)"
        authenticationService.verify(phoneNumber: countryCodePhoneNumber).subscribe(onNext: { [weak self] (verificationString) in
            guard let self = self else { return }
            self.router?.routeToVerifyNumber(phoneNumber: verificationString)
        }, onError: { [weak self]  error in
            self?.presenter.show(alert: AlertViewModel(alertViewType: .error, title: "Authorization failed", subtitle: error.localizedDescription, icon: nil) )
        }).disposeOnDeactivate(interactor: self)
    }
    
    func loginRequestSuccess(data: Bool) {
        listener?.loginSuccess()
    }

    func loginRequestFailed(error: Error) {
        router?.routeAwayFromVerifyNumber()
    }
}

public extension SignedOutPresentableListener {
    func didEnter(phoneNumber: String, countryCode: CountryCode) {
        print("Default implementation - \(#function)")
    }
}

public extension PhoneNumberVerificationListener {

    func loginRequestSuccess(data: Bool) {
        print("Default implementation - \(#function)")
    }
    
    func loginRequestFailed(error: Error) {
        print("Default implementation - \(#function)")
    }

}

public extension SignedOutPresentable {
    func show(alert: AlertViewModelling) {
        print("Default implementation - \(#function)")
    }
}
