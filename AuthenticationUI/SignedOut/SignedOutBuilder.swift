//
//  SignedOutBuilder.swift
//   2020
//

import RIBs
import UserInterface
import AuthenticationCommon

public protocol SignedOutDependency: AuthenticationCoreDependency { }

public final class SignedOutComponent: Component<SignedOutDependency>, PhoneNumberVerificationDependency {
    
    public var userService: UserServicing {
        return dependency.userService
    }

    public var permissionsManager: PermissionManaging {
        return dependency.permissionsManager
    }
    
    public var themeStream: ThemeStreaming {
        return dependency.themeStream
    }
    
    public var keyboardObserver: KeyboardObserving {
        return dependency.keyboardObserver
    }
    
    public var authenticationService: AuthenticationServicing {
        return dependency.authenticationService
    }

    fileprivate var phoneNumberVerificationBuilder: PhoneNumberVerificationBuildable {
        return PhoneNumberVerificationBuilder(dependency: self)
    }
}

// MARK: - Builder

public protocol SignedOutBuildable: Buildable {
    func build(withListener listener: SignedOutListener) -> SignedOutRouting
}

public final class DefaultSignedOutBuilder: Builder<SignedOutDependency>, SignedOutBuildable {

    public override init(dependency: SignedOutDependency) {
        super.init(dependency: dependency)
    }

    public func build(withListener listener: SignedOutListener) -> SignedOutRouting {
        let component = SignedOutComponent(dependency: dependency)
        let viewController = SignedOutViewController(themeStream: component.dependency.themeStream, keyboardObserver: component.dependency.keyboardObserver)
        let interactor = SignedOutInteractor(presenter: viewController, authenticationService: component.dependency.authenticationService, keyboardObserver: component.dependency.keyboardObserver)
        interactor.listener = listener
        return SignedOutRouter(interactor: interactor, viewController: viewController, verifyNumberBuilder: component.phoneNumberVerificationBuilder)
    }
}
