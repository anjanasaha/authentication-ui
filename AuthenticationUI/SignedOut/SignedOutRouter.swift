//
//  SignedOutRouter.swift
//   2020
//

import RIBs

public protocol SignedOutInteractable: Interactable, PhoneNumberVerificationListener {
    var router: SignedOutRouting? { get set }
    var listener: SignedOutListener? { get set }
}

public protocol SignedOutViewControllable: ViewControllable {
    // TODO: Declare methods the router invokes to manipulate the view hierarchy.
}

final class SignedOutRouter: ViewableRouter<SignedOutInteractable, SignedOutViewControllable>, SignedOutRouting {

    private let verifyNumberBuilder: PhoneNumberVerificationBuildable
    private var verifyNumberRouter: PhoneNumberVerificationRouting?
    
    init(interactor: SignedOutInteractable, viewController: SignedOutViewControllable, verifyNumberBuilder: PhoneNumberVerificationBuildable) {
        self.verifyNumberBuilder = verifyNumberBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    func routeToVerifyNumber(phoneNumber: String) {
        let router = verifyNumberBuilder.build(withListener: interactor)
        verifyNumberRouter = router
        attachChild(router)
        if let navigationController = viewControllable.uiviewController.navigationController {
            navigationController.pushViewController(router.viewControllable.uiviewController, animated: true)
        }
    }
    
    func routeAwayFromVerifyNumber() {
        guard let router = verifyNumberRouter else { return }
        router.viewControllable.uiviewController.dismiss(animated: true, completion: nil)
        verifyNumberRouter = nil
    }
}


public extension SignedOutRouting {
    
    func routeToVerifyNumber(phoneNumber: String) {
        print("Default implementation - \(#function)")
    }
    
    func routeAwayFromVerifyNumber() {
        print("Default implementation - \(#function)")
    }
}
