//
//  SignedOutViewController.swift
//   2020
//

import RIBs
import RxSwift
import UserInterface

public protocol SignedOutPresentableListener: class {
    func didEnter(phoneNumber: String, countryCode: CountryCode)
}

final class SignedOutViewController: ViewController, SignedOutViewControllable {

    weak var listener: SignedOutPresentableListener?
    
    private let placeholder = "Enter your mobile number"
    private let countryPickerView: UIImageView
    private let phoneNumberField: TextField
    private let logoView: ImageView
    private let textFormatter = USPhoneNumberTextFormatter()
    private let keyboardObserver: KeyboardObserving?
    
    override init(themeStream: ThemeStreaming, keyboardObserver: KeyboardObserving? = nil) {
        self.keyboardObserver = keyboardObserver
        self.logoView = ImageView(themeStream: themeStream)
        self.countryPickerView = ImageView(themeStream: themeStream)
        self.phoneNumberField = TextField(themeStream: themeStream, style: .regularPrimary)
        super.init(themeStream: themeStream)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func construct() {

        super.construct()

        logoView.image = UIImage(named: "Hopshop-Logo")

        countryPickerView.image = UIImage(named: "usa")
        countryPickerView.contentMode = .scaleAspectFill

        phoneNumberField.placeholder = placeholder
        phoneNumberField.keyboardType = .phonePad
        phoneNumberField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        phoneNumberField.defaultTextAttributes.updateValue(0.75.dip,
                                                           forKey: NSAttributedString.Key.kern)
        phoneNumberField.delegate = self

        view.addSubview(phoneNumberField)
        view.addSubview(countryPickerView)
        view.addSubview(logoView)

    }

    override func layout() {
        super.layout()

        countryPickerView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(view.safeAreaLayoutGuide).offset(leadingGutter)
            maker.centerY.equalTo(phoneNumberField)
            maker.width.equalTo(5.dip)
            maker.height.equalTo(3.dip)
        }

        phoneNumberField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(countryPickerView.snp.trailingMargin).offset(2.dip)
            maker.trailing.equalTo(view.safeAreaLayoutGuide).offset(trailingGutter)
            maker.top.equalTo(logoView.snp.bottom).offset(topGutter * 0.66)
            maker.height.equalTo(8.dip)
        }

        logoView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide)
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset( leadingGutter )
            $0.trailing.lessThanOrEqualTo(view.safeAreaLayoutGuide)
        }
    }
    
    @objc private func textFieldDidChange(textField: UITextField) {

        guard let text = textField.text else { return }

        let sanitizedText = text.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)

        if sanitizedText.count == textFormatter.maxDigits {
            listener?.didEnter(phoneNumber: sanitizedText, countryCode: .US)
        }
    }
}

extension SignedOutViewController: SignedOutPresentable {
    func show(alert: AlertViewModelling) {
        Alert(viewModel: alert).show(in: self)
    }
}

extension SignedOutViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let textField = textField as? TextField, let text = textField.text else { return }
        if text.count == 0 {
            textField.style = .largePrimary
            textField.placeholder = ""
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let textField = textField as? TextField, let text = textField.text else { return }
        if text.count == 0 {
            textField.style = .regularPrimary
            textField.placeholder = placeholder
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textField = textField as? TextField, let text = textField.text else { return true }
        let result = textFormatter.shouldChangeCharacters(replacement: string, text: text)
        textField.text = result.toText
        return result.shouldChange
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let textField = textField as? TextField, let text = textField.text else { return true }
        return text.count == 12
    }
}
